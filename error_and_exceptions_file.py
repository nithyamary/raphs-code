from sys import exc_info
def fileReader(filePath, fileUser):
	# What if the file doesn't exist?
	try:
		document = open(filePath)
	except IOError, exception:
		writeToStandardErrorAndExit("Could not open file '%s' because of error number '%d' with message '%s'", filePath, exception.errno, exception.strerror)
	try:
		fileUser.useFile(document)
		# handle exceptions from fileUser
	except FileUsageException, exception:
		writeToStandardErrorAndExit("Could not use file '%s' because '%s'", filePath, exception.message)
	except IOError, exception:
		writeToStandardErrorAndExit("Could not read file '%s' because of error number '%d' with message '%s'", filePath, exception.errno, exception.strerror)
	except:
		exceptionType, value, traceback = exc_info()
		writeToStandardError("Unexpected exception using file '%s' with type '%s' with value '%s'", filePath, exceptionType, value)
		raise
	finally:
		document.close()


from sys import exit
FailureExitCode = 1
def writeToStandardErrorAndExit(messageFormatString, filePath, *arguments):
	writeToStandardError(messageFormatString, filePath, *arguments)
	exit(FailureExitCode)

from sys import stderr
from os import linesep
def writeToStandardError(messageFormatString, filePath, *arguments):
	formatArguments = (filePath,) + arguments
	stderr.write((messageFormatString + linesep) % formatArguments)
 
class FileUsageException(Exception):

	def __init__(self, messageFormatString, *arguments):
		self.message = messageFormatString % arguments
