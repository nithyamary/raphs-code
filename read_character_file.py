

class ReadCharacter():

	Comma = ","
	DoubleQuoteMark = '"'
	LineFeed = '\n'
	CarriageReturn = '\r'

	def __init__(self, character):
		self.character = character	

	def isComma(self):
		return self._isCharacter(ReadCharacter.Comma)

	def isDoubleQuoteMark(self):
		return self._isCharacter(ReadCharacter.DoubleQuoteMark)

	def isLineFeed(self):
		return self._isCharacter(ReadCharacter.LineFeed)

	def isCarriageReturn(self):
		return self._isCharacter(ReadCharacter.CarriageReturn)

	def isEndOfFile(self):
		return self.character == ""

	def _isCharacter(self, like):
		return self.character == like