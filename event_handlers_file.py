class PrintingEventHandler():

	def useField(self, fieldValue):
		print "Field Value '%s'" % fieldValue

	def endOfRow(self, rowEnding):
		print "End of row"

	def endOfFile(self):
		print "End of file"

class MultiplyDispatchingEventHandler():

	def __init__(self, *eventHandlers):
		self.eventHandlers = eventHandlers

	def useField(self, fieldValue):
		for eventHandler in self.eventHandlers:
			eventHandler.useField(fieldValue)

	def endOfRow(self, rowEnding):
		for eventHandler in self.eventHandlers:
			eventHandler.endOfRow(rowEnding)

	def endOfFile(self):
		for eventHandler in self.eventHandlers:
			eventHandler.endOfFile()

class TwoDimensionalArrayBuildingEventHandler():

	def __init__(self):
		self.twoDimensionalArray = []
		self._newCurrentRow()

	def useField(self, fieldValue):
		self.currentRow.append(fieldValue)

	def endOfRow(self, rowEnding):
		self.twoDimensionalArray.append(self.currentRow)
		self._newCurrentRow()

	def endOfFile(self):
		pass

	def _newCurrentRow(self):
		self.currentRow = []

class PeopleEventHandler():

	def __init__(self):
		self.people = []
		self._newCurrentRow()

	def useField(self, fieldValue):
		self.currentRow.append(fieldValue)

	def endOfRow(self, rowEnding):
		self.people.append(Person(*self.currentRow))
		self._newCurrentRow()

	def endOfFile(self):
		pass

	def _newCurrentRow(self):
		self.currentRow = []

class CsvWritingEventHandler():
	pass	

class Person():

	def __init__(self, eyeColour, hairColour, height, dateOfBirth):
		self.eyeColour = eyeColour
		self.hairColour = hairColour
		self.height = height
		self.dateOfBirth = dateOfBirth

	def __repr__(self):
		return "Person(%s, %s, %s, %s)" % (self.eyeColour, self.hairColour, self.height, self.dateOfBirth)